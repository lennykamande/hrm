<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Dependants extends Model
{
    //

    protected $table = 'dependants';

    protected $fillable = [ 'name', 'dob', 'user_id', 'relation'];
}
