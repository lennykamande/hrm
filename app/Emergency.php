<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Emergency extends Model
{
    //
    protected $table = 'emergency_contact';

    protected $fillable = [ 'name', 'phone', 'user_id', 'relation'];
}
