<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Certification;
use App\Http\Requests;

class CertificationController extends Controller
{
    //
     public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
    	$cert = Certification::all();
        return view('certification.index', ['cert' => $cert]);
    }
}
