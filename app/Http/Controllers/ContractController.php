<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Contract;
use App\Http\Requests;
use App\Http\Requests\AddContract;
class ContractController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
    	$pays = Contract::all();
        return view('contract.index', ['pays' => $pays]);
    }

    /**
     * Create the jobs application.
     *
     * 
     */

    public function new(Request $request)
    {
        $user = $request->user()->country;

        return view('contract.new');
    }

    /**
     * Store the jobs application.
     *
     * 
     */

    public function store(AddContract $request)
    {
        Contract::create($request->all());

        return redirect('contract');
    }

    /**
     * Store the jobs application.
     *
     * 
     */

    public function edit($id)
    {
        $grade = Contract::findOrFail($id);

        return view('contract.edit', ['grade' => $grade]);
    }

    public function destroy($id)
    {
        $grade = Contract::findOrFail($id);

        $grade->delete();

        Session::flash('flash_message', 'Task successfully deleted!');

        return redirect()->route('contract.index');
    }
}
