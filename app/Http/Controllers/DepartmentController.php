<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Department;
use App\Http\Requests;
use App\Http\Requests\AddDepartment;

class DepartmentController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
    	$department = Department::all();
        return view('department.index', ['department' => $department]);
    }

    public function new()
    {
        return view('department.new');
    }
    
    /**
     * Store the jobs application.
     *
     * 
     */
    public function store(AddDepartment $request)
    {
        Department::create($request->all());

        return redirect('location');
    }

    /**
     * Store the jobs application.
     *
     * 
     */

    public function edit($id)
    {
        $grade = Department::findOrFail($id);

        return view('pay.edit', ['grade' => $grade]);
    }

    public function destroy($id)
    {
        $grade = Department::findOrFail($id);

        $grade->delete();

        Session::flash('flash_message', 'Task successfully deleted!');

        return redirect()->route('pay.index');
    }
}
