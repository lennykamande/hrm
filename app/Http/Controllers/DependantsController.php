<?php

namespace App\Http\Controllers;

use App\Dependants;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Requests\AddDependant;


class DependantsController extends Controller
{
    //
     public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */

    public function index(Request $request)
    {
    	$user = $request->user()->id;
    	$jobs = Dependants::where('user_id', '=', $user)->get();
        return view('dependants.index', ['jobs' => $jobs]);
    }

    /**
     * Create the jobs application.
     *
     * 
     */

    public function new(Request $request)
    {
       
        return view('dependants.new');
    }

    /**
     * Store the jobs application.
     *
     * 
     */

    public function store(AddDependant $request)
    {
        $user_id = $request->user()->id;
        Dependants::create($request->all() + ['user_id' => $user_id]);
        return redirect('dependant');
    }
}
