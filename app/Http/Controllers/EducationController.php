<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Education;
use App\Http\Requests;

class EducationController extends Controller
{
    //
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
    	$education = Education::all();
    	//return $permission;
        return view('education.index', ['education' => $education]);
    }
}
