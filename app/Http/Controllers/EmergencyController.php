<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Emergency;
use App\Http\Requests;
use App\Http\Requests\AddContact;

class EmergencyController extends Controller
{
    //
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */

    public function index(Request $request)
    {
    	$user = $request->user()->id;
    	$jobs = Emergency::where('user_id', '=', $user)->get();
        return view('emergency.index', ['jobs' => $jobs]);
    }

    /**
     * Create the jobs application.
     *
     * 
     */

    public function new(Request $request)
    {
        $user = $request->user()->id;
        return view('emergency.new', ['id' => $user]);
    }

    /**
     * Store the jobs application.
     *
     * 
     */

    public function store(AddContact $request)
    {
        $user_id = $request->user()->id;
        Emergency::create($request->all() + ['user_id' => $user_id]);
        return redirect('emergency');
    }
}
