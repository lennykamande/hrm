<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Jobs;
use App\Http\Requests;
use App\Http\Requests\AddJobs;

class JobsController extends Controller
{
    //
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
    	$jobs = Jobs::all();
        return view('jobs.index', ['jobs' => $jobs]);
    }

    /**
     * Create the jobs application.
     *
     * 
     */

    public function new(Request $request)
    {
        $user = $request->user()->country;

        return view('jobs.new');
    }

    /**
     * Store the jobs application.
     *
     * 
     */

    public function store(AddJobs $request)
    {
        Jobs::create($request->all());

        return redirect('jobs.index');
    }
}
