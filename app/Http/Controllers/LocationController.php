<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Location;
use App\Http\Requests;
use App\Http\Requests\AddLocation;

class LocationController extends Controller
{
    //
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
    	$permission = Location::all();
    	//return $permission;
        return view('Location.index', ['permission' => $permission]);
    }
    /**
     * Create the Location application.
     *
     * 
     */

    public function create(Request $request)
    {
        $user = $request->user()->country;

        return view('Location.new');
    }

    /**
     * Store the jobs application.
     *
     * 
     */

    public function store(AddLocation $request)
    {
        Location::create($request->all());

        return redirect('location');
    }

    /**
     * Store the jobs application.
     *
     * 
     */

    public function edit($id)
    {
        $grade = Location::findOrFail($id);

        return view('pay.edit', ['grade' => $grade]);
    }

    public function destroy($id)
    {
        $grade = Location::findOrFail($id);

        $grade->delete();

        Session::flash('flash_message', 'Task successfully deleted!');

        return redirect()->route('pay.index');
    }
}
