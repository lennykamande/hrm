<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Pay;
use App\Http\Requests;
use App\Http\Requests\AddPays;

class PayController extends Controller
{
    //
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
    	$pays = Pay::all();
        return view('pay.index', ['pays' => $pays]);
    }

    /**
     * Create the jobs application.
     *
     * 
     */

    public function create(Request $request)
    {
        $user = $request->user()->country;

        return view('pay.new');
    }

    /**
     * Store the jobs application.
     *
     * 
     */

    public function store(AddPays $request)
    {
        Pay::create($request->all());

        return redirect('pays');
    }

    /**
     * Store the jobs application.
     *
     * 
     */

    public function edit($id)
    {
        $grade = Pay::findOrFail($id);

        return view('pay.edit', ['grade' => $grade]);
    }

    public function destroy($id)
    {
        $grade = Pay::findOrFail($id);

        $grade->delete();

        Session::flash('flash_message', 'Task successfully deleted!');

        return redirect()->route('pay.index');
    }
}
