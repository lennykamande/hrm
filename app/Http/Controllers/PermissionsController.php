<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Permission;

use App\Http\Requests;

class PermissionsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
    	$permission = Permission::all();
        return view('permissions.index', ['permission' => $permission]);
    }
}
