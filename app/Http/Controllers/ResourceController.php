<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Resource;
use App\Http\Requests;

class ResourceController extends Controller
{
    //

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $grade = Resource::all();
        return view('empgrade.index', ['grade' => $grade]);

    }

    public function create(Request $request)
    {
        $user = $request->user()->country;

        return view('empgrade.new');
    }

    /**
     * Store the jobs application.
     *
     * 
     */

    public function store(Requests $request)
    {
        Resource::create($request->all());

        return redirect('employementgrade');
    }

    /**
     * Store the jobs application.
     *
     * 
     */

    public function edit($id)
    {
        $grade = Resource::findOrFail($id);

        return view('empgrade.edit', ['grade' => $grade]);
    }

    public function destroy($id)
    {
        $grade = Resource::findOrFail($id);

        $grade->delete();

        Session::flash('flash_message', 'Task successfully deleted!');

        return redirect()->route('empgrade.index');
    }
}
