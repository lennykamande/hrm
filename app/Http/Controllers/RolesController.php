<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Role;

use App\Http\Requests;

class RolesController extends Controller
{
     public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
    	$roles = Role::all();
        return view('roles.index', ['roles' => $roles]);
    }
}
