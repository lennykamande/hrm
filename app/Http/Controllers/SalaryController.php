<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Salary;
use App\Http\Requests;
use App\Http\Requests\AddSalary;

class SalaryController extends Controller
{
    //
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
    	$salary = Salary::all();
        return view('salary.index', ['salary' => $salary]);
    }

    /**
     * Create the salary application.
     *
     * 
     */

    public function new(Request $request)
    {
        $user = $request->user()->country;

        return view('salary.new');
    }

    /**
     * Store the jobs application.
     *
     * 
     */

    public function store(AddSalary $request)
    {
        Salary::create($request->all());

        return redirect('contracts');
    }

    /**
     * Store the jobs application.
     *
     * 
     */

    public function edit($id)
    {
        $grade = Salary::findOrFail($id);

        return view('pay.edit', ['grade' => $grade]);
    }

    public function destroy($id)
    {
        $grade = Salary::findOrFail($id);

        $grade->delete();

        Session::flash('flash_message', 'Task successfully deleted!');

        return redirect()->route('pay.index');
    }
}
