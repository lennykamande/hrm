<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Sessions;
use App\Trainings;
use App\Http\Requests;
use App\Http\Requests\AddSession;

class SessionsController extends Controller
{
    //
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
    	$ses = Sessions::with('training')->get();
        return view('sessions.index', ['ses' => $ses]);
    }


    public function new(Request $request)
    {
        $user = $request->user()->country;
        $training = Trainings::pluck('display_name', 'id');
        return view('sessions.new', ['training' => $training]);
    }

    /**
     * Store the jobs application.
     *
     * 
     */

    public function store(AddSession $request)
    {
        Sessions::create($request->all());

        return redirect('training-sessions');
    }
}
