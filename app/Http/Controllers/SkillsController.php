<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Skills;
use App\Http\Requests;

class SkillsController extends Controller
{
    //
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
    	$skills = Skills::all();
    	//return $permission;
        return view('skills.index', ['skills' => $skills]);
    }
}
