<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Trainings;
use App\Http\Requests;
use App\Http\Requests\AddTraining;

class TrainingsController extends Controller
{
    //
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
    	$training = Trainings::all();
        return view('training.index', ['training' => $training]);
    }


    public function new(Request $request)
    {
        $user = $request->user()->country;

        return view('training.new');
    }

    /**
     * Store the jobs application.
     *
     * 
     */

    public function store(AddTraining $request)
    {
        Trainings::create($request->all());

        return redirect('training');
    }
}
