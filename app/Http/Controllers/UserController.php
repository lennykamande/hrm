<?php

namespace App\Http\Controllers;


use DB;
use App\User;
use App\Jobs;
use App\Department;
use App\Location;
use App\Resource;
use App\Contract;
use Illuminate\Http\Request;
use Hash;
use App\Http\Requests;
use App\Http\Requests\AddUser;

class UserController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
    	$users = User::all();
        return view('users.index', ['users' => $users]);
    }

    public function new()
    {
        $department = Department::pluck('display_name', 'id');
        $job = Jobs::pluck('display_name', 'id');
        $country = Location::pluck('display_name', 'id');
        $resource = Resource::pluck('display_name', 'id');
        $contract = Contract::pluck('name', 'id');
        $line = User::select('id', DB::raw('CONCAT(fname, " ", lname) AS full_name'))
                ->orderBy('fname')
                ->pluck('full_name', 'id');
        return view('users.new', ['department' => $department, 'job' => $job, 'line' => $line, 'country' => $country, 'resource' => $resource, 'contract' => $contract]);
    }

        public function store(Request $request)
    {
        $request->merge(['password' => Hash::make($request->password)]);

        $user = User::create($request->all());

        return redirect('/users');
    }

    public function info(Request $request)
    {
        $user = $request->user()->id;

        $profile = User::where('id', '=', $user)->with('work','office')->get()->first();

        //return $profile;

        return view('users.info', ['profile' => $profile]);
    }
    public function employee($id)
    {
        $profile = User::findorFail('id','=', $id)->with('work','office')->get()->first();
        return view('users.employees', ['profile' => $profile]);
    }
}
