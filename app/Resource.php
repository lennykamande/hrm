<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Resource extends Model
{
    //

    protected $table = 'resource_category';

    protected $fillable = ['name', 'display_name', 'description'];
}
