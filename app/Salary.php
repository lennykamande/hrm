<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Salary extends Model
{
    //
    protected $table ='salary'; 

    protected $fillable = [ 'name', 'display_name', 'description', 'type', 'total', 'company_payable' ];
}
