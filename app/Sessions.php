<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sessions extends Model
{
    //
    protected $table = 'trainings_sessions';

    protected $fillable = [ 'name', 'display_name', 'startdate', 'enddate', 'training_id'];


	public function training()
	{
	    return $this->belongsTo('App\Trainings', 'training_id', 'id');
	}

}
