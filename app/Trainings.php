<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Trainings extends Model
{
    //

    protected $table = 'trainings';

    protected $fillable = [ 'name', 'display_name', 'description', 'coordinator', 'duration', 'cost'];
}
