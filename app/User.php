<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Klaravel\Ntrust\Traits\NtrustUserTrait;

class User extends Authenticatable
{
    use Notifiable, NtrustUserTrait;
    // add this trait to your user model

    /*
     * Role profile to get value from ntrust config file.
     */
    protected $roleProfile = 'user';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
     'email', 'password','fname', 'mname', 'lname', 'dob', 'emp_date', 'department', 'country', 'line_manager', 'job', 'resource', 'contract'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function work()
    {
        return $this->belongsTo('App\Jobs', 'job', 'id');
    }

    public function office()
    {
        return $this->belongsTo('App\Location', 'country', 'id');
    }
}
