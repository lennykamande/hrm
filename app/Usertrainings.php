<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Usertrainings extends Model
{
    //

    protected $table = 'user_training';

    protected $fillable = [ 'user_id', 'session_id'];

    public function trainings()
	{
	    return $this->belongsTo('App\sessions', 'session_id', 'id');
	}

	public function employees()
	{
		return $this->belongsTo('App\User', 'user_id', 'id');
	}

}
