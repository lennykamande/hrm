<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTrainingsSessionsMigration extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('trainings_sessions', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->unique();
            $table->string('display_name')->nullable();
            $table->string('training_id');
            $table->date('startdate');
            $table->date('enddate');
            $table->string('description')->nullable();
            $table->timestamps();
        });

        // Create table for associating session to users (Many-to-Many)
        Schema::create('session_user', function (Blueprint $table) {
            $table->integer('user_id')->unsigned();
            $table->integer('session_id')->unsigned();

            $table->foreign('user_id')->references('id')->on('users')
                ->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('session_id')->references('id')->on('trainings_sessions')
                ->onUpdate('cascade')->onDelete('cascade');

            $table->primary(['user_id', 'session_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('trainings_sessions');
        Schema::dropIfExists('session_user');
    }
}
