@extends('layouts.apps')

@section('content')

<h3><i class="fa fa-angle-right"></i> <a href="/emergency"> Emergency</a> / New Emergency Contact</h3>

   	<div class="row mt">
          		<div class="col-lg-12">
                  <div class="form-panel">
                  	{!! Form::open(['url' => '/newcontact', null, 'class' => 'form-horizontal style-form']) !!}

                      
                      	
                          <div class="form-group">

                            {!! Form::label('name', 'Contact Name', ['class' => 'col-sm-2 col-sm-2 control-label']) !!}
                            
                              <div class="col-sm-8">
                             {!! Form::text('name', null, ['class' => 'form-control', 'placeholder' => 'Julius Caesar']) !!}
                              </div>
                          </div>

                          <div class="form-group">

                            {!! Form::label('relation', 'Relation', ['class' => 'col-sm-2 col-sm-2 control-label']) !!}
                            
                              <div class="col-sm-8">
                             {!! Form::text('relation', null, ['class' => 'form-control', 'placeholder' => 'Father / Mother/ Wife / Husband']) !!}
                              </div>
                          </div>

                          <div class="form-group">

                            {!! Form::label('phone', 'Phone Number', ['class' => 'col-sm-2 col-sm-2 control-label']) !!}
                            
                              <div class="col-sm-8">
                             {!! Form::text('phone', null, ['class' => 'form-control', 'placeholder' => '0722222222']) !!}
                              </div>
                          </div>

                          
                        
                          <div class="form-group">
                           <div class="col-sm-8 col-sm-offset-2">

                            {!! Form::submit('Save', ['class'=> 'btn btn-primary btn-lg btn-block']) !!}
                            
                           
                          </div>
                          </div>
                {!! Form::close() !!} 
                  </div>
          		</div><!-- col-lg-12-->      	
          	</div><!-- /row -->

          	@endsection