@extends('layouts.apps')

@section('content')

<h3><i class="fa fa-angle-right"></i> Edit Grades</h3>

   	<div class="row mt">
          		<div class="col-lg-12">
                  <div class="form-panel">
                  	{!! Form::model($grade, ['method' => 'PATCH', 'url' => ['/update-grades'.$grade->id] , null, 'class' => 'form-horizontal style-form']) !!}

                      
                      	
                          <div class="form-group">

                            {!! Form::label('name', 'Grade Name', ['class' => 'col-sm-2 col-sm-2 control-label']) !!}
                            
                              <div class="col-sm-8">
                             {!! Form::text('name', null, ['class' => 'form-control', 'placeholder' => 'AAHI2']) !!}
                              </div>
                          </div>

                          <div class="form-group">

                            {!! Form::label('display_name', 'Display Name', ['class' => 'col-sm-2 col-sm-2 control-label']) !!}
                            
                              <div class="col-sm-8">
                             {!! Form::text('display_name', null, ['class' => 'form-control', 'placeholder' => 'AAHI2']) !!}
                              </div>
                          </div>

                          <div class="form-group">

                            {!! Form::label('description', 'Description', ['class' => 'col-sm-2 col-sm-2 control-label']) !!}
                            
                              <div class="col-sm-8">
                             {!! Form::textarea('description', null, ['class' => 'form-control', 'placeholder' => 'Description Name']) !!}
                              </div>
                          </div>
                        
                            <div class="form-group">
                           <div class="col-sm-8 col-sm-offset-2">

                            {!! Form::submit('Save', ['class'=> 'btn btn-primary btn-lg btn-block']) !!}
                            
                           
                          </div>
                          </div>
                {!! Form::close() !!} 
                  </div>
          		</div><!-- col-lg-12-->      	
          	</div><!-- /row -->

          	@endsection