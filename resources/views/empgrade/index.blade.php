
@extends('layouts.apps')

@section('content')

          	<h3><i class="fa fa-angle-right"></i> AAHI Grade</h3>
<div class="row mt">
                  <div class="col-md-12">

                      <div class="content-panel">
                        <div class="showback">

                          <a href="/new-resource" class="btn btn-success">New Grade</a>
                          
                        </div>
                          <table class="table table-striped table-advance table-hover">
	                  	  	  <h4> Grades</h4>
	                  	  	  <hr>
                              <thead>
                              <tr>
                                  <th><i class="fa fa-user"></i> Display-Name</th>
                                  <th class="hidden-phone"><i class="fa fa-envelope-o"></i> Description	</th>
                                  <th><i class="fa fa-bookmark"></i> Name</th>
                                  <th><i class=" fa fa-edit"></i> Status</th>
                                  <th></th>
                              </tr>
                              </thead>
                              <tbody>
                            	@foreach ($grade as $grades)
                              <tr>
                                  <td><a href="#">{{$grades->display_name}}</a></td>
                                  <td class="hidden-phone">{{$grades->description}}</td>
                                  <td>{{ $grades->name }}</td>
                                  <td><span class="label label-success label-mini">Active</span></td>
                                  <td>
                                       <a href="/get-grade/{{$grades->id}}" class="btn btn-primary btn-xs"><i class="fa fa-pencil"></i></a>
                                   <a href="/delete-grade/{{$grades->id}}" class="btn btn-danger btn-xs"><i class="fa fa-trash-o "></i></a>
                                  </td>
                              </tr>
                              @endforeach
                              </tbody>
                          </table>
                      </div><!-- /content-panel -->
                  </div><!-- /col-md-12 -->
              </div><!-- /row -->

@endsection