<div id="sidebar"  class="nav-collapse ">
              <!-- sidebar menu start-->
              <ul class="sidebar-menu" id="nav-accordion">
              
                  <p class="centered"><a href="profile.html"><img src="{{ asset('img/icon.png') }}" class="img-circle" width="60"></a></p>
                  <h5 class="centered">@if (Auth::check()) {{ Auth::user()->fname }} {{ Auth::user()->lname }} @else  @endif</h5>
                    
                  <li class="mt">
                      <a class="active" href="/">
                          <i class="fa fa-dashboard"></i>
                          <span>Dashboard</span>
                      </a>
                  </li>

                  @role('admin')
                  <li class="sub-menu">
                      <a href="javascript:;" >
                          <i class="fa fa-users"></i>
                          <span>E-I-M-S</span>
                      </a>
                      <ul class="sub">
                          <li><a  href="/users">Employee list</a></li>
                          <li><a  href="/new-employee">New Employee</a></li>
                          <li><a  href="/roles">User Roles</a></li>
                          <li><a  href="/permissions">Permissions</a></li>
                      </ul>
                  </li>

                  <li class="sub-menu">
                      <a href="javascript:;" >
                          <i class="fa fa-hdd-o"></i>
                          <span>Job</span>
                      </a>
                      <ul class="sub">
                          <li><a  href="/jobs">Jobs Titles</a></li>
                          <li><a  href="/contract">Contract Component</a></li>
                          <li><a  href="/pays">Pay Grades</a></li>
                          <li><a  href="/employementgrade">Resource Grade</a></li>
                      </ul>
                  </li>

                   <li class="sub-menu">
                      <a href="javascript:;" >
                          <i class="fa fa-desktop"></i>
                          <span>Organization</span>
                      </a>
                      <ul class="sub">
                          <li><a  href="/location">Locations</a></li>
                          <li><a  href="#">Structure</a></li>
                          <li><a  href="/department">Departments</a></li>
                      </ul>
                  </li>

                  <li class="sub-menu">
                      <a href="javascript:;" >
                          <i class="fa fa-cubes"></i>
                          <span>Qualification</span>
                      </a>
                      <ul class="sub">
                          <li><a  href="/skills">Skills</a></li>
                          <li><a  href="/education">Education</a></li>
                          <li><a  href="/certification">Certification</a></li>
                        
                      </ul>
                  </li>
                   <li class="sub-menu">
                      <a href="javascript:;" >
                          <i class="fa fa-paper-plane"></i>
                          <span>Leave</span>
                      </a>
                      <ul class="sub">
                          <li><a  href="/leave-calender">Leave Calender</a></li>
                          <li><a  href="#">Entitlements</a></li>
                          <li><a  href="#">Reports</a></li>
                          <li><a  href="#">Leave type</a></li>
                          <li><a  href="#">Leave list</a></li>
                      </ul>
                  </li> 

                  <li class="sub-menu">
                      <a href="javascript:;" >
                          <i class="fa fa-wrench"></i>
                          <span>Training</span>
                      </a>
                      <ul class="sub">
                          <li><a  href="/training">Courses</a></li>
                          <li><a  href="/training-sessions">Sessions</a></li>
                          <li><a  href="#">Reports</a></li>
                      </ul>
                  </li>
                  @endrole

                  @role('owner')
                  <li class="sub-menu">
                      <a href="javascript:;" >
                          <i class="fa fa-file-word-o"></i>
                          <span>Personal Info</span>
                      </a>
                      <ul class="sub">
                          <li><a  href="#">Personal Details</a></li>
                          <li><a  href="#">Emergency Contacts</a></li>
                          <li><a  href="#">Dependants</a></li>
                          <li><a  href="#">Salary Grade</a></li>
                          <li><a  href="#">Qualifications</a></li>
                      </ul>
                  </li>
              @endrole

              @role('users')
                  <li class="sub-menu">
                      <a href="javascript:;" >
                          <i class="fa fa-file-word-o"></i>
                          <span>Personal Info</span>
                      </a>
                      <ul class="sub">
                          <li><a  href="/info">Personal Details</a></li>
                          <li><a  href="/emergency">Emergency Contacts</a></li>
                          <li><a  href="/dependant">Dependants</a></li>
                          <li><a  href="#">Salary Grade</a></li>
                          <li><a  href="#">Qualifications</a></li>
                      </ul>
                  </li>

                  <li class="sub-menu">
                      <a href="javascript:;" >
                          <i class="fa fa-cogs"></i>
                          <span>Training</span>
                      </a>
                      <ul class="sub">
                          <li><a  href="#">My Trainings</a></li>
                          <li><a  href="#">Previous Trainings</a></li>
                      </ul>
                  </li>
              @endrole
              </ul>
              <!-- sidebar menu end-->
          </div>