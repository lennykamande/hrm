
@extends('layouts.apps')

@section('content')

          	<h3><i class="fa fa-angle-right"></i> Job Titles</h3>
<div class="row mt">
                  <div class="col-md-12">
                      <div class="content-panel">
                        <div class="showback">
                          <a href="/new-job" class="btn btn-success">New Job</a>
                        </div>
                          <table class="table table-striped table-advance table-hover">
	                  	  	  <h4> Job Title</h4>
	                  	  	  <hr>
                              <thead>
                              <tr>
                                  <th><i class="fa fa-user"></i> Jobs</th>
                                  <th class="hidden-phone"><i class="fa fa-envelope-o"></i> Description	</th>
                                  <th><i class="fa fa-bookmark"></i> Job</th>
                                  <th><i class=" fa fa-edit"></i> Status</th>
                                  <th></th>
                              </tr>
                              </thead>
                              <tbody>
                            	@foreach ($jobs as $job)
                              <tr>
                                  <td><a href="#">{{$job->name}}</a></td>
                                  <td class="hidden-phone">{{$job->description}}</td>
                                  <td>{{$job->display_name}}</td>
                                  <td><span class="label label-success label-mini">Active</span></td>
                                  <td>
                                      <button class="btn btn-primary btn-xs"><i class="fa fa-pencil"></i></button>
                                      <button class="btn btn-danger btn-xs"><i class="fa fa-trash-o "></i></button>
                                  </td>
                              </tr>
                              @endforeach
                              </tbody>
                          </table>
                      </div><!-- /content-panel -->
                  </div><!-- /col-md-12 -->
              </div><!-- /row -->

@endsection