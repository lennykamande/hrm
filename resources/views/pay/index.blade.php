
@extends('layouts.apps')

@section('content')

          	<h3><i class="fa fa-angle-right"></i> Grading Structure</h3>
<div class="row mt">
                  <div class="col-md-12">
                      <div class="content-panel">
                        <div class="showback">

                          <a href="/new-grade" class="btn btn-success">New Grade</a>
                          
                        </div>
                          <table class="table table-striped table-advance table-hover">
	                  	  	  <h4> Roles</h4>
	                  	  	  <hr>
                              <thead>
                              <tr>
                                  <th><i class="fa fa-user"></i> Roles</th>
                                  <th class="hidden-phone"><i class="fa fa-envelope-o"></i> Description	</th>
                                  <th><i class="fa fa-bookmark"></i> Job</th>
                                  <th><i class=" fa fa-edit"></i> Status</th>
                                  <th></th>
                              </tr>
                              </thead>
                              <tbody>
                            	@foreach ($pays as $pays)
                              <tr>
                                  <td><a href="#">{{$pays->display_name}}</a></td>
                                  <td class="hidden-phone">{{$pays->description}}</td>
                                  <td>12000.00$ </td>
                                  <td><span class="label label-success label-mini">Active</span></td>
                                  <td>
                                   <a href="/get-grade/{{$pays->id}}" class="btn btn-primary btn-xs"><i class="fa fa-pencil"></i></a>
                                   <a href="/delete-grade/{{$pays->id}}" class="btn btn-danger btn-xs"><i class="fa fa-trash-o "></i></a>
                                  
                                  </td>
                              </tr>
                              @endforeach
                              </tbody>
                          </table>
                      </div><!-- /content-panel -->
                  </div><!-- /col-md-12 -->
              </div><!-- /row -->

@endsection