@extends('layouts.apps')

@section('content')

<h3><i class="fa fa-angle-right"></i> <a href="/training-sessions"> Training Session</a> / New Sessions</h3>

   	<div class="row mt">
          		<div class="col-lg-12">
                  <div class="form-panel">
                  	{!! Form::open(['url' => '/newsession', null, 'class' => 'form-horizontal style-form']) !!}

                      
                      	
                          <div class="form-group">

                            {!! Form::label('name', 'Session Name', ['class' => 'col-sm-2 col-sm-2 control-label']) !!}
                            
                              <div class="col-sm-8">
                             {!! Form::text('name', null, ['class' => 'form-control', 'placeholder' => 'FA001']) !!}
                              </div>
                          </div>

                          <div class="form-group">

                            {!! Form::label('display_name', 'Display Name', ['class' => 'col-sm-2 col-sm-2 control-label']) !!}
                            
                              <div class="col-sm-8">
                             {!! Form::text('display_name', null, ['class' => 'form-control', 'placeholder' => 'Induction']) !!}
                              </div>
                          </div>

                          <div class="form-group">

                            {!! Form::label('training_id', 'Training Course', ['class' => 'col-sm-2 col-sm-2 control-label']) !!}
                            
                              <div class="col-sm-8">
                             {!! Form::select('training_id', $training, null, ['class' => 'form-control', 'placeholder' => '']) !!}
                              </div>
                          </div>

                          <div class="form-group">

                            {!! Form::label('startdate', 'Start Date', ['class' => 'col-sm-2 col-sm-2 control-label']) !!}
                            
                              <div class="col-sm-8">
                             {!! Form::date('startdate', \Carbon\Carbon::now(), ['class' => 'form-control', 'placeholder' => 'Company Covered']) !!}
                              </div>
                          </div>

                          <div class="form-group">

                            {!! Form::label('enddate', 'End Date', ['class' => 'col-sm-2 col-sm-2 control-label']) !!}
                            
                              <div class="col-sm-8">
                             {!! Form::date('enddate', \Carbon\Carbon::now(), ['class' => 'form-control', 'placeholder' => 'Company Covered']) !!}
                              </div>
                          </div>

                          <div class="form-group">

                            {!! Form::label('description', 'Session Description', ['class' => 'col-sm-2 col-sm-2 control-label']) !!}
                            
                              <div class="col-sm-8">
                             {!! Form::textarea('description', null, ['class' => 'form-control', 'placeholder' => 'Reason For Course']) !!}
                              </div>
                          </div>
                        
                          <div class="form-group">
                           <div class="col-sm-8 col-sm-offset-2">

                            {!! Form::submit('Save', ['class'=> 'btn btn-primary btn-lg btn-block']) !!}
                            
                           
                          </div>
                          </div>
                {!! Form::close() !!} 
                  </div>
          		</div><!-- col-lg-12-->      	
          	</div><!-- /row -->

          	@endsection