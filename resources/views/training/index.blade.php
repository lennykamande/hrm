
@extends('layouts.apps')

@section('content')

          	<h3><i class="fa fa-angle-right"></i> Trainings  </h3>
<div class="row mt">
                  <div class="col-md-12">
                      <div class="content-panel">
                        <div class="showback">

                          <a href="/new-training" class="btn btn-success">New Training</a>
                          
                        </div> 
                          <table class="table table-striped table-advance table-hover">
	                  	  	  <h4>Training Schedule</h4>
	                  	  	  <hr>
                              <thead>
                              <tr>
                                  <th><i class="fa fa-graduation-cap"></i> Code</th>
                                  <th class="hidden-phone"><i class="fa fa-envelope-o"></i> Description	</th>
                                  <th><i class="fa fa-bookmark"></i> Type</th>
                                  <th><i class="fa fa-coffee"></i> Coordinator</th>
                                  <th><i class=" fa fa-edit"></i> Status</th>
                                  <th></th>
                              </tr>
                              </thead>
                              <tbody>
                            	@foreach ($training as $skill)
                              <tr>
                                  <td><a href="#">{{$skill->display_name}}</a></td>
                                  <td>{{$skill->name}}</td>
                                  <td class="hidden-phone">{{$skill->description}}</td>
                                  <td>{{$skill->coordinator}}</td>
                                  <td><span class="label label-success label-mini">Active</span></td>
                                  <td>
                                      <button class="btn btn-primary btn-xs"><i class="fa fa-pencil"></i></button>
                                      <button class="btn btn-danger btn-xs"><i class="fa fa-trash-o "></i></button>
                                  </td>
                              </tr>
                              @endforeach
                              </tbody>
                          </table>
                      </div><!-- /content-panel -->
                  </div><!-- /col-md-12 -->
              </div><!-- /row -->

@endsection