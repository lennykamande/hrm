@extends('layouts.apps')

@section('content')

<h3><i class="fa fa-angle-right"></i> <a href="/training"> Training</a> / New Training</h3>

   	<div class="row mt">
          		<div class="col-lg-12">
                  <div class="form-panel">
                  	{!! Form::open(['url' => '/newtraining', null, 'class' => 'form-horizontal style-form']) !!}

                      
                      	
                          <div class="form-group">

                            {!! Form::label('display_name', 'Course Code', ['class' => 'col-sm-2 col-sm-2 control-label']) !!}
                            
                              <div class="col-sm-8">
                             {!! Form::text('display_name', null, ['class' => 'form-control', 'placeholder' => 'FA001']) !!}
                              </div>
                          </div>

                          <div class="form-group">

                            {!! Form::label('name', 'Course Name', ['class' => 'col-sm-2 col-sm-2 control-label']) !!}
                            
                              <div class="col-sm-8">
                             {!! Form::text('name', null, ['class' => 'form-control', 'placeholder' => 'Induction']) !!}
                              </div>
                          </div>

                          <div class="form-group">

                            {!! Form::label('coordinator', 'Coordinator', ['class' => 'col-sm-2 col-sm-2 control-label']) !!}
                            
                              <div class="col-sm-8">
                             {!! Form::text('coordinator', null, ['class' => 'form-control', 'placeholder' => 'Human Resource Dept']) !!}
                              </div>
                          </div>

                          <div class="form-group">

                            {!! Form::label('cost', 'Cost', ['class' => 'col-sm-2 col-sm-2 control-label']) !!}
                            
                              <div class="col-sm-8">
                             {!! Form::text('cost', null, ['class' => 'form-control', 'placeholder' => 'Company Covered']) !!}
                              </div>
                          </div>

                          <div class="form-group">

                            {!! Form::label('duration', 'Duration', ['class' => 'col-sm-2 col-sm-2 control-label']) !!}
                            
                              <div class="col-sm-8">
                             {!! Form::text('duration', null, ['class' => 'form-control', 'placeholder' => '1 Day']) !!}
                              </div>
                          </div>

                        <div class="form-group">

                            {!! Form::label('description', 'Course Description', ['class' => 'col-sm-2 col-sm-2 control-label']) !!}
                            
                              <div class="col-sm-8">
                             {!! Form::textarea('description', null, ['class' => 'form-control', 'placeholder' => '1 Day']) !!}
                              </div>
                          </div>

                          
                        
                          <div class="form-group">
                           <div class="col-sm-8 col-sm-offset-2">

                            {!! Form::submit('Save', ['class'=> 'btn btn-primary btn-lg btn-block']) !!}
                            
                           
                          </div>
                          </div>
                {!! Form::close() !!} 
                  </div>
          		</div><!-- col-lg-12-->      	
          	</div><!-- /row -->

          	@endsection