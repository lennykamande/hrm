@extends('layouts.apps')

@section('content')

 <h3><i class="fa fa-angle-right"></i> Personal Information</h3>
<div class="row mt">
     <div class="col-md-12">
        <div class="showback">
                <h4><i class="fa fa-angle-right"></i> Profile Information</h4>
                <div class="row">
                <div class="col-md-4 col-md-offset-4">
                <p><img src="{{asset('img/ui-zac.jpg')}}" class="img-circle" width="100"></p>
                <p><b> {{$profile->fname}} {{$profile->mname}} {{$profile->lname}} </b></p>
              </div>
            </div>
                  <div class="row">

                    <div class="col-md-4">
                      <p class="small mt">FIRST NAME</p>
                      <p>{{$profile->fname}}</p>
                      <p> </p>
                    </div>

                    <div class="col-md-4">
                      <p class="small mt">MIDDLE NAME</p>
                      <p>{{$profile->mname}}</p>
                      <p> </p>
                    </div>

                    <div class="col-md-4">
                      <p class="small mt">LAST NAME</p>
                      <p>{{$profile->lname}}</p>
                      <p> </p>
                    </div>

                  </div>
                    
                  <div class="row">

                    <div class="col-md-4">
                      <p class="small mt">DATE OF BIRTH</p>
                      <p>{{$profile->dob}}</p>
                      <p> </p>
                    </div>

                    <div class="col-md-4">
                      <p class="small mt">POSITION</p>
                      <p>{{$profile->work->name}}</p>
                      <p> </p>
                    </div>

                    <div class="col-md-4">
                      <p class="small mt">OFFICE</p>
                      <p>{{$profile->office->name}}</p>
                      <p> </p>
                    </div>

                  </div>

                  <div class="row">

                    <div class="col-md-4">
                      <p class="small mt">EMPLOYMENT DATE</p>
                      <p>{{$profile->emp_date}}</p>
                      <p> </p>
                    </div>

                    <div class="col-md-4">
                      <p class="small mt">POSITION</p>
                      <p>{{$profile->work->name}}</p>
                      <p> </p>
                    </div>

                    <div class="col-md-4">
                      <p class="small mt">OFFICE</p>
                      <p>{{$profile->office->name}}</p>
                      <p> </p>
                    </div>


                  </div>
                  
              </div>
      </div><!-- /col-md-12 -->
  </div><!-- /row -->

@endsection