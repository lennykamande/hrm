@extends('layouts.apps')

@section('content')

<h3><i class="fa fa-angle-right"></i> <a href="/employee"> Employees </a> / New Employee</h3>

   	<div class="row mt">
          		<div class="col-lg-12">
                  <div class="form-panel">
                  	{!! Form::open(['url' => '/newuser', null, 'class' => 'form-horizontal style-form']) !!}

                      
                      	
                          <div class="form-group">

                            {!! Form::label('fname', 'First Name', ['class' => 'col-sm-2 col-sm-2 control-label']) !!}
                            
                              <div class="col-sm-8">
                             {!! Form::text('fname', null, ['class' => 'form-control', 'placeholder' => 'First Name']) !!}
                              </div>
                          </div>

                          <div class="form-group">

                            {!! Form::label('mname', 'Middle Name', ['class' => 'col-sm-2 col-sm-2 control-label']) !!}
                            
                              <div class="col-sm-8">
                             {!! Form::text('mname', null, ['class' => 'form-control', 'placeholder' => 'Middle Name']) !!}
                              </div>
                          </div>

                          <div class="form-group">

                            {!! Form::label('lname', 'Last Name', ['class' => 'col-sm-2 col-sm-2 control-label']) !!}
                            
                              <div class="col-sm-8">
                             {!! Form::text('lname', null, ['class' => 'form-control', 'placeholder' => 'Last Name']) !!}
                              </div>
                          </div>

                          <div class="form-group">

                            {!! Form::label('email', 'Email', ['class' => 'col-sm-2 col-sm-2 control-label']) !!}
                            
                              <div class="col-sm-8">
                             {!! Form::text('email', null , ['class' => 'form-control', 'placeholder' => 'dwanjala@actionafricahelp.org']) !!}
                              </div>
                          </div>

                          <div class="form-group">

                            {!! Form::label('dob', 'D.O.B', ['class' => 'col-sm-2 col-sm-2 control-label']) !!}
                            
                              <div class="col-sm-8">
                             {!! Form::date('dob', \Carbon\Carbon::now(), ['class' => 'form-control']) !!}
                              </div>
                          </div>

                          <div class="form-group">

                            {!! Form::label('department', 'Department', ['class' => 'col-sm-2 col-sm-2 control-label']) !!}
                            
                              <div class="col-sm-8">
                             {!! Form::select('department', $department, null, ['class' => 'form-control', 'placeholder' => 'Select A Department']) !!}
                              </div>
                          </div>

                          <div class="form-group">

                            {!! Form::label('Job', 'Job', ['class' => 'col-sm-2 col-sm-2 control-label']) !!}
                            
                              <div class="col-sm-8">
                             {!! Form::select('job', $job, null, ['class' => 'form-control']) !!}
                              </div>
                          </div>

                          <div class="form-group">

                            {!! Form::label('linemanager', 'Line Manager', ['class' => 'col-sm-2 col-sm-2 control-label']) !!}
                            
                              <div class="col-sm-8">
                             {!! Form::select('linemanager', $line, null, ['class' => 'form-control']) !!}
                              </div>
                          </div>

                          <div class="form-group">

                            {!! Form::label('country', 'Office', ['class' => 'col-sm-2 col-sm-2 control-label']) !!}
                            
                              <div class="col-sm-8">
                             {!! Form::select('country', $country, null, ['class' => 'form-control']) !!}
                              </div>
                          </div>

                          <div class="form-group">

                            {!! Form::label('resource', 'Resource Category', ['class' => 'col-sm-2 col-sm-2 control-label']) !!}
                            
                              <div class="col-sm-8">
                             {!! Form::select('resource', $resource, null, ['class' => 'form-control']) !!}
                              </div>
                          </div>

                          <div class="form-group">

                            {!! Form::label('emp_date', 'Employment Date', ['class' => 'col-sm-2 col-sm-2 control-label']) !!}
                            
                              <div class="col-sm-8">
                             {!! Form::date('emp_date', \Carbon\Carbon::now(), ['class' => 'form-control']) !!}
                              </div>
                          </div>

                          <div class="form-group">

                            {!! Form::label('contract', 'Contract Type', ['class' => 'col-sm-2 col-sm-2 control-label']) !!}
                            
                              <div class="col-sm-8">
                             {!! Form::select('contract', $contract, null, ['class' => 'form-control']) !!}
                              </div>
                          </div>

                          <div class="form-group">

                            {!! Form::label('password', 'Password', ['class' => 'col-sm-2 col-sm-2 control-label']) !!}
                            
                              <div class="col-sm-8">
                             {!! Form::text('password', '123456', ['class' => 'form-control']) !!}
                              </div>
                          </div>

                      <div class="form-group">
                           <div class="col-sm-8 col-sm-offset-2">

                            {!! Form::submit('Save', ['class'=> 'btn btn-primary btn-lg btn-block']) !!}
                            
                           
                          </div>
                          </div>
                {!! Form::close() !!} 
                  </div>
          		</div><!-- col-lg-12-->      	
          	</div><!-- /row -->

          	@endsection