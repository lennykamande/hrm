<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('/', function () {
    return view('home');
});

Auth::routes();

Route::get('home', 'HomeController@index');
Route::get('users', 'UserController@index');
Route::get('user/{id}', 'UserController@employee');
Route::get('roles', 'RolesController@index');
Route::get('permissions', 'PermissionsController@index');
Route::get('jobs', 'JobsController@index');
Route::get('salay', 'SalaryController@index');
Route::get('pays', 'PayController@index');
Route::get('employementgrade', 'ResourceController@index');
Route::get('location', 'LocationController@index');
Route::get('skills', 'SkillsController@index');
Route::get('education', 'EducationController@index');
Route::get('certification', 'CertificationController@index');
Route::get('training', 'TrainingsController@index');
Route::get('training-sessions', 'SessionsController@index');
Route::get('department', 'DepartmentController@index');
Route::get('contract', 'ContractController@index');
Route::get('info', 'UserController@info');
Route::get('emergency', 'EmergencyController@index');
Route::get('dependant', 'DependantsController@index');
Route::get('leave-calender', 'LeaveController@index');

//creating information 
Route::get('new-employee', 'UserController@new');
Route::get('new-job', 'JobsController@new');
Route::get('new-grade', 'PayController@create');
Route::get('new-salary', 'SalaryController@new');
Route::get('new-contract', 'ContractController@new');
Route::get('new-resource', 'ResourceController@create');
Route::get('new-location', 'LocationController@create');
Route::get('new-department', 'DepartmentController@create');
Route::get('new-contact', 'EmergencyController@new');
Route::get('new-dependant', 'DependantsController@new');
Route::get('new-training', 'TrainingsController@new');
Route::get('new-sessions', 'SessionsController@new');

//store new information
Route::post('newuser', 'UserController@store');
Route::post('newjob', 'JobsController@store');
Route::post('newgrade', 'PayController@store');
Route::post('newsalary', 'SalaryController@store');
Route::post('newcontract', 'ContractController@store');
Route::post('newlocation', 'LocationController@store');
Route::post('newdepartment', 'DepartmentController@store');
Route::post('newcontact', 'EmergencyController@store');
Route::post('newdependant', 'DependantsController@store');
Route::post('newtraining', 'TrainingsController@store');
//edit Information


Route::get('session-user/{id}', 'SessionsController@edit');
Route::get('get-grade/{id}', 'PayController@edit');
Route::delete('delete-grade/{id}', 'PayController@destroy');